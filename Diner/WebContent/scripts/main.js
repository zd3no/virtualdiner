
/**
 * Add items to local storage
 */
var data = {
    set : function(key, value) {
	if (!key || !value) {
	    return;
	}

	if (typeof value === "object") {
	    value = JSON.stringify(value);
	}
	localStorage.setItem(key, value);
    },
    get : function(key) {
	var value = localStorage.getItem(key);

	if (!value) {
	    return;
	}

	// assume it is an object that has been stringified
	if (value[0] === "{" || value[0] === "[") {
	    value = JSON.parse(value);
	}

	return value;
    }
};

/**
 * Capitalizes the string
 * @returns
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/**
 * Updates the badge on the screen
 */
function updateBadge() {
    if (data.get("badgeCounter") >= 0) {
	document.getElementById("badgeCounter").innerHTML = data
		.get("badgeCounter");
    }else{
	document.getElementById("badgeCounter").innerHTML = "0";
    }
    ;
}
window.onresize = adjustSize;

/**
 * Adjusts the size of the window
 */
function adjustSize() {
    var winWidth = window.innerWidth;
    if (winWidth < 1280) {
	document.getElementById("holder").style.width = (winWidth - 20) + "px";
    } else {
	document.getElementById("holder").style.width = "1280px";
    }
}

/**
 * Called when user clicks on button_clear in contact.html
 * Clears the fields of the form
 */
function btnClear() {
    document.getElementById("name").value = "";
    document.getElementById("nameError").style.display = "none";
    document.getElementById("email").value = "";
    document.getElementById("emailError").style.display = "none";
    document.getElementById("phone").value = "";
    document.getElementById("message").value = "";
    
}

/**
 * Called when user clicks on button_send in contact.html.
 * Sends an email to the business email address
 */
function btnSend() {
	document.getElementById("email").setCustomValidity("");
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var message = document.getElementById("message").value;
    document.getElementById("sendingEmailtext").innerHTML = "Sending email...";
    //JS validation
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!(document.getElementById("name").checkValidity() &&
    		document.getElementById("email").checkValidity() &&
    		document.getElementById("phone").checkValidity() &&
    		document.getElementById("message").checkValidity()))
    	return;
    if(re.test(email) == false){document.getElementById("email").setCustomValidity("Invalid Email");return;}
    document.getElementById("background123").style.display = "block";
    document.getElementById("circularG").style.display = "block";
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }//end else
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    var response = xmlhttp.responseText;
	    if (response == "OK"){
		console.log("Email sent");
		document.getElementById("circularG").style.display = "none";
		document.getElementById("name").value = "";
		document.getElementById("email").value = "";
		document.getElementById("phone").value = "";
		document.getElementById("message").value = "";
		document.getElementById("sendingEmailtext").innerHTML = "Email sent successfully";
		window.setTimeout(function(){document.getElementById("background123").style.display = "none";}, 2000);
	    }else{
		alert(response);
	    }//end else
	}//end if
    };//end function
    xmlhttp.open("POST","rest/email/sendEmail",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("FROM="+email+"&NAME="+name+"&PHONE="+phone+"&MESSAGE="+message);
    
    window.setTimeout(function(){
    	document.getElementById("circularG").style.display = "none";
    	document.getElementById("sendingEmailtext").innerHTML = "Connection timed out";
    }, 7000);
    window.setTimeout(function(){document.getElementById("background123").style.display = "none";}, 9000);
}//end btnSend()

/**
 * Reduces the ordered element amount by one
 * @param element
 */
function reduceQuantity(element) {
    var number;
	number = document.getElementById(element).value;
	if (number > 0) {
	    number--;
	    document.getElementById(element).value = number;
	}
}

/**
 * Adds one to the ordered element
 * @param element
 */
function increaseQuantity(element) {
    var number;
	number = document.getElementById(element).value;
	number++;
	document.getElementById(element).value = number;
}

/**
 * Function to create menuItem objects of the order.
 * @param name
 * @param size
 * @param price
 * @param amount
 * @param id
 */
function menuItem(name, size, price, amount, id) {
    this.name = name;
    this.size = size;
    this.price = parseFloat(price);
    this.amount = parseInt(amount);
    this.total = parseFloat(price) * parseInt(amount);
    this.id = id;
}

/**
 * Adds the selected items to the order
 * @param array - Array of up to 3 different sizes of same menu item
 * @param name - The name of the menu item
 */
function add_to_plate(array, name){
	if(array.length == 3){
		var element1 = array[0];
		var element2 = array[1];
		var element3 = array[2];
	}else if(array.length == 2){
		var element1 = array[0];
		var element2 = array[1];
	}else{
		var element1 = array[0];
	}
    if (element1 != null && document.getElementById(element1).value > 0) {
	var price = document.getElementById(element1 + "price").innerHTML;
	var amount = document.getElementById(element1).value;
	var size = document.getElementById(element1 + "size").innerHTML;
	// create a new manuItem object
	var newMenuItem = new menuItem(name.replace("\\\'", "'"), size, price, amount, parseInt(element1.substring(4, element1.length)));
	// update badgeCounter
	    document.getElementById("badgeCounter").innerHTML = parseInt(document
		    .getElementById("badgeCounter").innerHTML)
		    + parseInt(amount);
	    if (data.get("badgeCounter") >= 0) {
		data.set("badgeCounter", parseInt(data.get("badgeCounter"))
			+ parseInt(amount));
	    } else {
		data.set("badgeCounter", parseInt(amount));
	    }
	    console.log(data.get("badgeCounter"));
	    // adds new MenuItem to localStorage. If the same item is already present, update the amount
	    if(typeof data.get(element1) === "object" ){
		item = data.get(element1);
		xamount= parseInt(item.amount) + parseInt(amount);
		console.log("updated amount = "+ xamount);
		newItem = new menuItem(name.replace("\\\'", "'"), size, price, xamount, parseInt(element1.substring(4, element1.length)));
		console.log("newItem = "+ newItem);
		localStorage.removeItem(element1);
		data.set(element1, newItem);
	    }else{
		data.set(element1, newMenuItem);
	    }
	    // clear the amount from textbox
	    document.getElementById(element1).value = 0;
	    itemAddedPopup();
    }//end if
    if (element2 != null && document.getElementById(element2).value > 0) {
	var price = document.getElementById(element2 + "price").innerHTML;
	var amount = document.getElementById(element2).value;
	var size = document.getElementById(element2 + "size").innerHTML;
	// create a new manuItem object
	var newMenuItem = new menuItem(name.replace("\\\'", "'"), size, price, amount, parseInt(element2.substring(4, element2.length)));
	// update badgeCounter
	    document.getElementById("badgeCounter").innerHTML = parseInt(document
		    .getElementById("badgeCounter").innerHTML)
		    + parseInt(amount);
	    if (data.get("badgeCounter") >= 0) {
		data.set("badgeCounter", parseInt(data.get("badgeCounter"))
			+ parseInt(amount));
	    } else {
		data.set("badgeCounter", parseInt(amount));
	    }
	    console.log(data.get("badgeCounter"));
	   // adds new MenuItem to localStorage. If the same item is already present, update the amount
	    if(typeof data.get(element2) === "object" ){
		item = data.get(element2);
		xamount= parseInt(item.amount) + parseInt(amount);
		console.log("updated amount = "+ xamount);
		newItem = new menuItem(name.replace("\\\'", "'"), size, price, xamount, parseInt(element2.substring(4, element2.length)));
		console.log("newItem = "+ newItem);
		localStorage.removeItem(element2);
		data.set(element2, newItem);
	    }else{
		data.set(element2, newMenuItem);
	    }
	    // clear the amount from textbox
	    document.getElementById(element2).value = 0;
	    itemAddedPopup();
    }//end if
    if (element3 != null && document.getElementById(element3).value > 0) {
	var price = document.getElementById(element3 + "price").innerHTML;
	var amount = document.getElementById(element3).value;
	var size = document.getElementById(element3 + "size").innerHTML;
	// create a new menuItem object
	var newMenuItem = new menuItem(name.replace("\\\'", "'"), size, price, amount, parseInt(element3.substring(4, element3.length)));
	// update badgeCounter
	    document.getElementById("badgeCounter").innerHTML = parseInt(document
		    .getElementById("badgeCounter").innerHTML)
		    + parseInt(amount);
	    if (data.get("badgeCounter") >= 0) {
		data.set("badgeCounter", parseInt(data.get("badgeCounter"))
			+ parseInt(amount));
	    } else {
		data.set("badgeCounter", parseInt(amount));
	    }
	    console.log(data.get("badgeCounter"));
	    // adds new MenuItem to localStorage. If the same item is already present, update the amount
	    if(typeof data.get(element3) === "object" ){
		item = data.get(element3);
		xamount= parseInt(item.amount) + parseInt(amount);
		console.log("updated amount = "+ xamount);
		newItem = new menuItem(name.replace("\\\'", "'"), size, price, xamount, parseInt(element3.substring(4, element3.length)));
		console.log("newItem = "+ newItem);
		localStorage.removeItem(element3);
		data.set(element3, newItem);
	    }else{
		data.set(element3, newMenuItem);
	    }
	    // clear the amount from textbox
	    document.getElementById(element3).value = 0;
	    itemAddedPopup();
    }//end if
}//end add_to_plate(array, name)

/**
 * append fragment function.
 * source: http://stackoverflow.com/questions/814564/inserting-html-elements-with-javascript
 */
function create(htmlStr) {
    var frag = document.createDocumentFragment(), temp = document
	    .createElement('div');
    temp.innerHTML = htmlStr;
    while (temp.firstChild) {
	frag.appendChild(temp.firstChild);
    }
    return frag;
}

/**
 * Populates Plate.html from localStorage
 */
function populatePlate() {
    // check if there are items in local storage
    if (localStorage.length > 1) {
	var article = document.getElementById("main_article");
	var fragmentString = '<div class = "flex-column" id="new_row">	'
		+ '<table id="myTable">' + '<tr>' + '<th>Size</th>'
		+ '<th>Name</th>' + '<th>Amount</th>' + '<th>Price</th>'
		+ '<th>Total</th>' + '<th>Action</th>' + '</tr>';
	// populate table from localstorage
	var total = 0.0;
	for (var i=0; i<localStorage.length; i++) {
	    var key = localStorage.key(i);
	    if(key.indexOf("item") == 0){
		console.log("If condition passed with i= " + i);
		var item = data.get(key);
		console.log(item);
		fragmentString += '<tr id="row'+i+'"><td><div class="itemSize">'+item.size+'</div></td>';
		fragmentString += '<td><div class="itemName"><b>'+item.name+'</b></div></td>';
		fragmentString += '<td><div class="itemAmount">'+item.amount+'</div></td>';
		fragmentString += '<td><div class="itemPrice">'+parseFloat(item.price).toFixed(2)+'</div></td>';
		fragmentString += '<td><div class="itemTotal"><b>'+parseFloat(item.total).toFixed(2)+' €</b></div></td>';
		fragmentString += '<td><input class="itemBtnRemove" id="itemBtnRemove" onclick="itemRemove(\''+key+'\',\''+i+'\')" type="submit" value="Remove"></input></td></tr>';
		total +=(parseInt(item.amount) * parseFloat(item.price));
	    }
	}//end for
	var totalRow='<tr><td colspan="6" style="text-align:center"><b>Grand total: <span id="grandTotal">'+parseFloat(total).toFixed(2)+'  €</span></b></td></tr>';
	var commentRow='<tr><td colspan="7"><textarea id="comment_area" rows="4" cols="60" name="comment" placeholder="If you have any comments..."></textarea></td></tr>';
	fragmentString += totalRow+commentRow+'</div></table><div style="margin-top:30px;margin-bottom:30px;">'+
	    '<input type="button" value="Clear" id="btnClear" onclick="clearOrder()" style="margin-right:20px"/>'+
	    '<input type="button" value="Order" id="btnOrder" onclick="sendOrder()" style="margin-left:20px"/></div>';
	var fragment = create(fragmentString);
	article.insertBefore(fragment, article.childNodes[0]);
    }else{//no items were ordered
	var article = document.getElementById("main_article");
	var fragment = create('<div class = "flex-column" style="min-width:600px;min-height:500px;"><h2>Your Plate is empty</h2></div>');
	article.insertBefore(fragment, article.childNodes[0]);
	article.style.background = "url(images/Dinner-plate-empty.png) no-repeat";
	article.style.backgroundSize = "100%";
    }//end if/else
}//end populatePlate()

/**
 * Removes the item[number] from local storage and deletes row[row] from table
 * @param key
 * @param rowNumber
 */
function itemRemove(key, rowNumber){
    var badgeCurrentAmount = data.get("badgeCounter");
    var item = data.get(key);
    var amount = item.amount;
    var price = item.price;
    var total = parseFloat(document.getElementById("grandTotal").innerHTML);
    total -= (parseFloat(price)*parseInt(amount));
    document.getElementById("grandTotal").innerHTML = total.toString() + " €";
    console.log("badgeCounter= "+badgeCurrentAmount);
    badgeCurrentAmount = parseInt(badgeCurrentAmount) - parseInt(amount);
    console.log("badgeCounter-amount = "+badgeCurrentAmount);
    data.set("badgeCounter", String(badgeCurrentAmount));
    updateBadge();
    
    localStorage.removeItem(key);
    var row = document.getElementById("row"+rowNumber);
    console.log("going to remove row"+rowNumber);
    row.remove();
    console.log("row"+(parseInt(rowNumber)+1)+" has been removed from table");
    
    var itemsFound = false;
    for(var i =0; i< localStorage.length; i++){
    	var lsKey = localStorage.key(i);
    	console.log("substring: "+lsKey.substring(0, 4));
    	if(lsKey.substring(0,4) == "item"){
    		itemsFound = true;
    		break;
    	}
    }
    if(!itemsFound){
    	console.log("No Items, going to remove");
    	localStorage.clear();
    	updateBadge();
    	var table = document.getElementsByTagName("table")[0];
        table.remove();
        var article = document.getElementById("main_article");
    	var fragment = create('<div class = "flex-column" style="min-width:600px;min-height:500px;"><h2>Your Plate is empty</h2></div>');
    	article.insertBefore(fragment, article.childNodes[0]);
    	article.style.background = "url(images/Dinner-plate-empty.png) no-repeat";
    	article.style.backgroundSize = "100%";
    	document.getElementById("new_row").style.display = 'none';
    }
}

/**
 * Clear the order
 */
function clearOrder(){
    localStorage.clear();
    updateBadge();
    var table = document.getElementsByTagName("table")[0];
    table.remove();
    var article = document.getElementById("main_article");
	var fragment = create('<div class = "flex-column" style="min-width:600px;min-height:500px;"><h2>Your Plate is empty</h2></div>');
	article.insertBefore(fragment, article.childNodes[0]);
	article.style.background = "url(images/Dinner-plate-empty.png) no-repeat";
	article.style.backgroundSize = "100%";
	document.getElementById("new_row").style.display = 'none';
}

/**
 * Send the order to server
 */
function sendOrder(){
	//add the comment to the local storage
	stringComment = document.getElementById("comment_area").value;
	if(stringComment.length <= 0){
		stringComment = "-no_comment-";
	}
	data.set("comment", stringComment);
    //check if the user is logged in
    if(getCookie("userName") != null){
	//SEND ORDER
	sendOrderToServer();
    }else{
	//save the sessionStorage item "from" to "sendOrder" so that the 
	//authenticate forms know to send the order after successful authentication
	sessionStorage.setItem("from", "sendOrder");
	
	var title = "Notice:";
	      var body = 'In order to complete the order, you need to authenticate yourself.<br><br><b>You will now be redirected.';
	      popup = '<div id="popup_background"><div id="popup_box"><div id="popup_title"><p>'+
	      title+'</p></div><div id="popup_message"><p>'+
	      body+'</p></div><div id="popup_buttons">'+
		  '</div></div></div>"';
		node = document.createElement("div");
		node.innerHTML = popup;
		document.body.appendChild(node);
		window.setTimeout(function(){window.location = "authenticate.html";}, 3000);
    }
}

/**
 * Compare fields for validation
 * @param input - Fields to compare (email or password)
 */
function compare(input) {
    if(input == "email"){
	var email = document.getElementById("email");
	var emailConfirm = document.getElementById("emailConfirm");
	if(email.value != emailConfirm.value){
	    emailConfirm.setCustomValidity("Email addresses do not match!");
	}else{
	    emailConfirm.setCustomValidity("");
	}
    }else if (input="password"){
	var email = document.getElementById("password");
	var emailConfirm = document.getElementById("passwordConfirm");
	if(email.value != emailConfirm.value){
	    emailConfirm.setCustomValidity("Passwords do not match!");
	}else{
	    emailConfirm.setCustomValidity("");
	}
    }
  }

/**
 * Called when user submits the login form in Authentication.html
 */
function login() {
    var emailString = document.getElementById("emailUser").value;
    var passwordString = document.getElementById("passwordUser").value;
    var password = document.getElementById("passwordUser");
    var email = document.getElementById("emailUser");
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    document.getElementById("emailError").style.display = "none";

    // perform JS side validation. If fails, exit, leave HTML5 validators to it
    password.setCustomValidity("");
    email.setCustomValidity("");
    if(email.checkValidity() == false || password.checkValidity() == false)
    	return;
    if(re.test(emailString) == false) {email.setCustomValidity("Invalid email");return;}
    if (passwordString.length < 6) {password.setCustomValidity("Password must be longer than 6 digits");return;}
    console.log("passed JS validation");

    // perform server validation
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    var response = xmlhttp.responseText;
	    // check if the response is a json object or plain string (error message)
	    if (response[0] === "{" || response[0] === "[") {
		console.log("Authentication successful");
		var parsedResponse = JSON.parse(response);
//get user's name from DB and save it in the cookie together with email
		var name = parsedResponse[0][1];
//cookieName,'email---userName',3days expiry
		setCookie("userName",emailString+"---"+name,3);
//check if you were redirected here when the user was trying to send the order
		if(sessionStorage.getItem("from") == "sendOrder"){
		    sessionStorage.clear();
		    //SEND ORDER
		    sendOrderToServer();
		}else{
		    window.location = "userArea.html";
		}//end else
	    } else {
		// not an object/ Display error message
		document.getElementById("emailError").innerHTML = response;
		document.getElementById("emailError").style.display = "block";
	    }//end else
	}//end if
    };
    xmlhttp.open("GET", "rest/getUserByEmailAndPassword?email="
	    + encodeURIComponent(emailString) + "&password=" + encodeURIComponent(passwordString), true);
    xmlhttp.send();
}//end login()

/**
 * Generates a new password for the user. The user will receive an email with
 * the newly generated password from the server.
 */
function lostPassword(){
    var email = document.getElementById("emailUser");
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //check if the email text field is valid and not empty
    if(email.value != "" && re.test(email.value)){
	var emailString = email.value;
	//RESEND GENERATED PASSWORD FROM SERVER
	//show the spinner
	document.getElementById("background123").style.display = "block";
	document.getElementById("circularG").style.display = "block";
	var xmlhttp;
	    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	    } else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    }
	    xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    var response = xmlhttp.responseText;
		    //check if the database has been queried correctly and password has been reset
		    if(response == "OK"){
			document.getElementById("background123").style.display = "none";
			document.getElementById("emailError").style.display = "none";
			var title = "Notice:";
			      var body = 'An email containing a temporary password has been sent to your email address.<br><br> Please log in with the new password.';
			      popup = '<div id="popup_background"><div id="popup_box"><div id="popup_title"><p>'+
			      title+'</p></div><div id="popup_message"><p>'+
			      body+'</p></div><div id="popup_buttons">'+
				  '<input type="submit" id="popup_buttonAffirmative" value="OK, I understand" onclick="document.getElementById(\'popup_background\').style.display = \'none\'">'+
				  '</div></div></div>"';
				node = document.createElement("div");
				node.innerHTML = popup;
				document.body.appendChild(node);
		    }else{
			document.getElementById("emailError").innerHTML = response;
			document.getElementById("emailError").style.display = "block";
			document.getElementById("background123").style.display = "none";
		    }//end else
		}//end if
	    };
	    xmlhttp.open("POST","rest/email/sendEmail/resetPassword",true);
	    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send("email="+emailString);
	    window.setTimeout(function(){
	    	document.getElementById("circularG").style.display = "none";
	    	document.getElementById("sendingEmailtext").innerHTML = "Connection timed out";
	    }, 7000);
	    window.setTimeout(function(){document.getElementById("background123").style.display = "none";}, 9000);
    }else{
	console.log("custom validity is going to get set");
	document.getElementById("emailError").innerHTML = "Please provide a valid email address";
	document.getElementById("emailError").style.display = "block";
    }//end if
}//end lostpassword()

/**
 * Register new user
 */
function register(){
    document.getElementById("registrationError").style.display = "none";
    var nameString = document.getElementById("name").value;
    var surnameString = document.getElementById("surname").value;
    var phoneString = document.getElementById("phone").value;
    var addressString = document.getElementById("address").value;
    var emailString = document.getElementById("email").value;
    var emailConfirmString = document.getElementById("emailConfirm").value;
    var passwordString = document.getElementById("password").value;
    var passwordConfirmString = document.getElementById("passwordConfirm").value;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    //JS validation
    if(!(document.getElementById("name").checkValidity() &&
    		document.getElementById("surname").checkValidity() &&
    		document.getElementById("phone").checkValidity() &&
    		document.getElementById("address").checkValidity() &&
    		document.getElementById("email").checkValidity() &&
    		document.getElementById("emailConfirm").checkValidity() &&
    		document.getElementById("password").checkValidity() &&
    		document.getElementById("passwordConfirm").checkValidity()))
    	return;
	if (passwordString != passwordConfirmString){
	    compare("password");
	    return;
	    };
	if (passwordString.length < 6){
	    document.getElementById("passwordConfirm").setCustomValidity("Password must be at least 6 characters long");
	    return;
	}else{
	    document.getElementById("passwordConfirm").setCustomValidity("");
	}
    document.getElementById("email").setCustomValidity("");
	if (emailString != emailConfirmString){
	    compare("email");
	    return;
	}
	if(re.test(emailString) == false){
		document.getElementById("email").setCustomValidity("Invalid Email!");
		return;
	}
        
    
    //INSERT NEW VALUES IN THE DB
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }//end else
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    var response = xmlhttp.responseText;
	    if (response == "OK"){
		console.log("User has been registered in the DB");
		setCookie("userName",emailString+"---"+nameString,3);
		//check if you were redirected here when the user was trying to send the order
		if(sessionStorage.getItem("from") == "sendOrder"){
		    sessionStorage.clear();
		    //SEND ORDER
		    sendOrderToServer();
		}else{
		    window.location = "userArea.html";
		}//end else
	    }else{
		document.getElementById("registrationError").innerHTML = response;
		document.getElementById("registrationError").style.display = "block";
	    }//end else
	}//end if
    };//end function
    xmlhttp.open("POST","rest/registerUser",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("name="+nameString+"&surname="+surnameString+"&phone="+phoneString+"&address="+addressString+"&email="+emailString+"&password="+passwordString);
}//end register()

/**
 * Add a new cookie
 * @param cname - cookie name
 * @param cvalue - cookie value
 * @param exdays - expire in days
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

/**
 * Returns a cookie value based on the cookie name
 * @param cname
 * @returns
 */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return null;
}

/**
 * Retrieves the username cookie and dislays the value on the screen.
 */
function userCheck(){
    var cookieString = getCookie("userName");
    if (cookieString != null){
	var list = cookieString.split("---");
	console.log("username cookie found with value: "+list[0]);
	console.log("User's name = "+list[1]);
	document.getElementById("username").style.display = "flex";
	document.getElementById("user").innerHTML = list[1];
    }else{
	console.log("no cookie found with name 'userName'");
    }
}

/**
 * Contact the server and extract the information needed to display 4 special offers
 */
function specialOffer(){
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }//end else
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    var response = xmlhttp.responseText;
	    var list = JSON.parse(response);
	    document.getElementById("imgSpecial1").setAttribute("src", "images/large/"+list[0][2]);
	    document.getElementById("special1Name").innerHTML = list[0][0]+"<p id=\"special1Price\" class=\"itemPrice\" style=\"margin: 3px 5px;\">€ "+list[0][3]+"</p>";
	    
	    document.getElementById("imgSpecial2").setAttribute("src", "images/large/"+list[1][2]);
	    document.getElementById("special2Name").innerHTML = list[1][0]+"<p id=\"special2Price\" class=\"itemPrice\" style=\"margin: 3px 5px;\">€ "+list[1][3]+"</p>";;
	    
	    document.getElementById("imgSpecial3").setAttribute("src", "images/large/"+list[2][2]);
	    document.getElementById("special3Name").innerHTML = list[2][0]+"<p id=\"special2Price\" class=\"itemPrice\" style=\"margin: 3px 5px;\">€ "+list[2][3]+"</p>";;
	    
	    document.getElementById("imgSpecial4").setAttribute("src", "images/large/"+list[3][2]);
	    document.getElementById("special4Name").innerHTML = list[3][0]+"<p id=\"special3Price\" class=\"itemPrice\" style=\"margin: 3px 5px;\">€ "+list[3][3]+"</p>";;
	}
    };
    xmlhttp.open("GET","rest/getSpecialOffer",true);
    xmlhttp.send();
}//end specialOffer()

/**
 * Gets different categories from the list of items
 * @param list - list of items with full description
 * @returns {Array} - array of categories
 */
function getCategories(list){
	var categories = new Array();;
    var counter = 0;
    for(var i=0; i<list.length; i++){
    	if(categories.length == 0){
    		categories[counter] = list[i].category;
    		counter++;
    	}else{
    		var found = false;
    		for(var j=0; j<categories.length; j++){
    			if(list[i].category == categories[j]){
    				found = true;
    				break;
    			}
    		}
    		if(!found){
    			categories[counter] = list[i].category;
    			counter++;
    		}
    	}
    }
    categories.sort();
    //remove leading digits 
    for(var i=0; i<categories.length; i++){
    	categories[i] = categories[i].substring(2,categories[i].length);
    }
    return categories;
}

/**
 * Returns the different sizes the item may have
 * @param list - list of items with full description
 * @param itemName - the name of the item we want to extract sizes
 * @returns {Array} - array of different sizes of the item
 */
function getItemSizes(list, itemName){
	var sizes = new Array();
	var counter = 0;
	for(var i=0; i<list.length; i++){
		if(list[i].name == itemName){
			sizes[counter] = list[i];
			counter++;
		}
	}
	return sizes;
}

/**
 * Returns a list of items with atomic values only. No repeating items.
 * @param list
 * @returns {Array}
 */
function getAtomicItemsList(list){
	var atomicList = new Array();
	var counter = 0;
	for(var i=0; i<list.length; i++){
		if(atomicList.length ==0){
			atomicList[counter] = list[i];
			counter++;
		}else{
			var found = false;
			for(var j=0; j<atomicList.length; j++){
				if(atomicList[j].name == list[i].name){
					found = true;
					break;
				}
			}
			if(!found){
				atomicList[counter] = list[i];
				counter++;
			}
		}
	}
	return atomicList;
}

/**
 * Populates Menu.html from database
 */
function populateMenu() {
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }//end else
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    var response = xmlhttp.responseText;
	    var list = JSON.parse(response);
	    //responseText returns an array[] of List<CompleteItem>
	    //individual objects are extracted by list[x], where x is the index of the object
	    //individual attributes of objects are extracted by list[x].attribute, where x is the index of the object and attribute is the name of the object's attribute
	    var categories = getCategories(list);
	    
	    var fragmentString = "<h2>Pick your choice</h2>\n" + 
	    		"<br>\n" + 
	    		"<!-- Menu Goes here -->\n" + 
	    		"<!-- First Category -->";
	    //loop through all elements
	    for(var cat=0; cat < categories.length; cat++){
fragmentString += "<details>\n" + 
		"<summary>"+categories[cat]+"</summary>\n"+
		"<!-- First Item From This Category -->";
		var atomicItemsList = getAtomicItemsList(list);
		for(var item=0; item<atomicItemsList.length; item++){
		    if((atomicItemsList[item].category).substring(2,(atomicItemsList[item].category).length) == categories[cat]){
fragmentString += 
"<details id=\"details_child\">\n" + 
"<summary id=\"summary_child\">\n" + 
"<div class=\"item_col\">\n" + 
"<p id=\"item_name\">"+atomicItemsList[item].name+"</p>\n" + 
"<p id=\"item_description_short\">"+atomicItemsList[item].descriptionShort+"</p>\n" + 
"</div>\n" + 
"</summary>\n" + 
"<div class=\"item_holder\">\n" + 
"<div class=\"item_holder_left\">\n" + 
"<img src=\"images/large/"+atomicItemsList[item].imageName+"\" />\n" + 
"<form>\n" + 
"<fieldset>\n"; 

//check if this item contains more sizes
var itemList = getItemSizes(list, atomicItemsList[item].name);

if(itemList.length == 3){
	fragmentString += "<div class=\"item_prices\">\n" + 
	    "<div class=\"left\">\n" + 
	    "<div class=\"h3\" id=\"item"+itemList[0].id+"size\">"+itemList[0].size.capitalize()+"</div>\n"+
	    "<div class=\"h3\" id=\"item"+itemList[1].id+"size\" style=\"display:block\">"+itemList[1].size.capitalize()+"</div>\n" + 
	    "<div class=\"h3\" id=\"item"+itemList[2].id+"size\" style=\"display:block\">"+itemList[2].size.capitalize()+"</div>\n" + 
	    "</div>\n" + 
	    "<div class=\"right\">\n" + 
	    "<div style=\"display:flex; height:47px\">\n" + 
	    "<div class='itemPrice' id=\"item"+itemList[0].id+"price\">"+itemList[0].price+"€</div>\n" + 
	    "<input id=\"addSize1\" onclick=\"increaseQuantity('item"+itemList[0].id+"')\" type=\"button\" value=\"+\"></input>\n" + 
	    "<input class=\"priceSize1TextBox\" id=\"item"+itemList[0].id+"\" type=\"text\" value=\"0\" maxlength=\"2\"></input>\n" + 
	    "<input id=\"removeSize1\" onclick=\"reduceQuantity('item"+itemList[0].id+"')\" type=\"button\" value=\"-\"></input>\n" + 
	    "</div>\n" + 
	    "<br>\n" + 
	    "<div style=\"display:flex; height:47px\">\n" + 
	    "<div class='itemPrice' id=\"item"+itemList[1].id+"price\">"+itemList[1].price+"€</div>\n" + 
	    "<input id=\"addSize2\" onclick=\"increaseQuantity('item"+itemList[1].id+"')\" type=\"button\" value=\"+\"></input>\n" + 
	    "<input class=\"priceSize2TextBox\" id=\"item"+itemList[1].id+"\" type=\"text\" value=\"0\" maxlength=\"2\"></input>\n" + 
	    "<input id=\"removeSize2\" onclick=\"reduceQuantity('item"+itemList[1].id+"')\" type=\"button\" value=\"-\"></input>\n" + 
	    "</div>\n" + 
	    "<br>\n" + 
	    "<div style=\"display:flex; height:47px\">\n" + 
	    "<div class='itemPrice' id=\"item"+itemList[2].id+"price\">"+itemList[2].price+"€</div>\n" + 
	    "<input id=\"addSize3\" onclick=\"increaseQuantity('item"+itemList[2].id+"')\" type=\"button\" value=\"+\"></input>\n" + 
	    "<input class=\"priceSize3TextBox\" id=\"item"+itemList[2].id+"\" type=\"text\" value=\"0\" maxlength=\"2\"></input>\n" + 
	    "<input id=\"removeSize3\" onclick=\"reduceQuantity('item"+itemList[2].id+"')\" type=\"button\" value=\"-\"></input>\n" + 
	    "</div>\n" + 
	    "</div>\n" + 
	    "</div>\n" +
	    "</fieldset>\n" + 
	    "</form>\n" + 
	    "<div style=\"text-align: center\">\n" + 
	    "	<input id=\"add_to_plate\" type=\"button\" value=\"Add\" onclick=\"add_to_plate(['item"+itemList[0].id+"','item"+itemList[1].id+"','item"+itemList[1].id+"'],\'"+itemList[0].name.replace("'", "\\\'")+"\')\" />\n" + 
	    "</div>\n" + 
	    "</div>\n";
}//end if
if(itemList.length == 2){
    
	fragmentString += "<div class=\"item_prices\">\n" + 
	    "<div class=\"left\">\n" + 
	    "<div class=\"h3\" id=\"item"+itemList[0].id+"size\">"+itemList[0].size.capitalize()+"</div>\n"+
	    "<div class=\"h3\" id=\"item"+itemList[1].id+"size\" style=\"display:block\">"+itemList[1].size.capitalize()+"</div>\n" + 
	    "</div>\n" + 
	    "<div class=\"right\">\n" + 
	    "<div style=\"display:flex; height:47px\">\n" + 
	    "<div class='itemPrice' id=\"item"+itemList[0].id+"price\">"+itemList[0].price+"€</div>\n" + 
	    "<input id=\"addSize1\" onclick=\"increaseQuantity('item"+itemList[0].id+"')\" type=\"button\" value=\"+\"></input>\n" + 
	    "<input class=\"priceSize1TextBox\" id=\"item"+itemList[0].id+"\" type=\"text\" value=\"0\" maxlength=\"2\"></input>\n" + 
	    "<input id=\"removeSize1\" onclick=\"reduceQuantity('item"+itemList[0].id+"')\" type=\"button\" value=\"-\"></input>\n" + 
	    "</div>\n" + 
	    "<br>\n" + 
	    "<div style=\"display:flex; height:47px\">\n" + 
	    "<div class='itemPrice' id=\"item"+itemList[1].id+"price\">"+itemList[1].price+"€</div>\n" + 
	    "<input id=\"addSize2\" onclick=\"increaseQuantity('item"+itemList[1].id+"')\" type=\"button\" value=\"+\"></input>\n" + 
	    "<input class=\"priceSize2TextBox\" id=\"item"+itemList[1].id+"\" type=\"text\" value=\"0\" maxlength=\"2\"></input>\n" + 
	    "<input id=\"removeSize2\" onclick=\"reduceQuantity('item"+itemList[1].id+"')\" type=\"button\" value=\"-\"></input>\n" + 
	    "</div>\n" + 
	    "</div>\n" +
	    "</fieldset>\n" + 
	    "</form>\n" + 
	    "<div style=\"text-align: center\">\n" + 
	    "	<input id=\"add_to_plate\" type=\"button\" value=\"Add\" onclick=\"add_to_plate(['item"+itemList[0].id+"','item"+itemList[1].id+"'],\'"+itemList[0].name.replace("'", "\\\'")+"\')\" />\n" + 
	    "</div>\n" + 
	    "</div>\n";
}//end if
if(itemList.length == 1){
    
    fragmentString += "<div class=\"item_prices\">\n" + 
    "<div class=\"left\">\n" + 
    "<div class=\"h3\" id=\"item"+itemList[0].id+"size\">"+itemList[0].size.capitalize()+"</div>\n"+
    "</div>\n" + 
    "<div class=\"right\">\n" + 
    "<div style=\"display:flex; height:47px\">\n" + 
    "<div class='itemPrice' id=\"item"+itemList[0].id+"price\">"+itemList[0].price+"€</div>\n" + 
    "<input id=\"addSize1\" onclick=\"increaseQuantity('item"+itemList[0].id+"')\" type=\"button\" value=\"+\"></input>\n" + 
    "<input class=\"priceSize1TextBox\" id=\"item"+itemList[0].id+"\" type=\"text\" value=\"0\" maxlength=\"2\"></input>\n" + 
    "<input id=\"removeSize1\" onclick=\"reduceQuantity('item"+itemList[0].id+"')\" type=\"button\" value=\"-\"></input>\n" + 
    "</div>\n" + 
    "</div>\n" + 
	"</fieldset>\n" + 
	"</form>\n" + 
	"<div style=\"text-align: center\">\n" + 
	"	<input id=\"add_to_plate\" type=\"button\" value=\"Add\" onclick=\"add_to_plate([\'item"+itemList[0].id+"\'],\'"+itemList[0].name.replace("'", "\\\'")+"\')\" />\n" + 
	"</div>\n" + 
	"</div>\n";
}//end if

fragmentString +=
"<div class=\"item_holder_right\">\n" + 
"	<h2>Description:</h2>\n" + 
"		<p id=\"item_description_long\">"+atomicItemsList[item].descriptionLong+"</p>\n" + 
"		<h2>Ingredients:</h2>\n" + 
"		<p id=\"item_ingredients\">"+atomicItemsList[item].ingredients+"</p>\n" + 
"		<h2>Allergy advice:</h2>\n" + 
"		<p id=\"item_allergy\">"+atomicItemsList[item].allergy+"</p>\n" + 
"		<h2>Nutritional information:</h2>\n" + 
"		<div class=\"nutrition_wrapper\">\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Kcalories</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].kcalories+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Protein</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].protein+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Carbs</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].carbs+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Fat</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].fat+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Saturates</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].saturates+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Fibre</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].fibre+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Sugar</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].sugar+"g</p>\n" + 
"			</div>\n" + 
"			<div class=\"nutrition_item\">\n" + 
"				<p id=\"nutrition_name\">Salt</p>\n" + 
"				<p id=\"nutrition_value\">"+atomicItemsList[item].salt+"g</p>\n" + 
"			</div>\n" + 
"		</div>\n" + 
"	</div>\n" + 
"</div>\n" + 
"</details>\n<!-- Next Item From This Category -->\n";
		    }//end if(item.category == category
		}//end for(var item in list)
		fragmentString += "</details>\n" + 
				"<!-- Next Category -->\n";
	    }//end for(var category in categories)
	    
	//insert the whole fragment string into the page's article
	var article = document.getElementById("main_article");
	var fragment = create(fragmentString);
	article.insertBefore(fragment, article.childNodes[0]);
	}//end if (xmlhttp.readyState
	var Summaries = document.getElementsByTagName('summary');
	for (var i = 0; i < Summaries.length; i++) {
	    details_shim(Summaries[i]);
	}
    };//end function
    xmlhttp.open("GET","rest/getAllItems",true);
    xmlhttp.send();
}//end populateMenu()                                                                                                                                                                                 

/**
 * Logs the user out of the application.
 */
function logout(){
    localStorage.clear();
    updateBadge();
    setCookie("userName","",0);
    window.location = "index.html";
}

/**
 * Check cookies for user details
 */
function authenticateRedirect(){
    var cookieString = getCookie("userName");
    if (cookieString == null){
	window.location = "authenticate.html";
    }else{
	var list = cookieString.split("---");
	document.getElementById("username").style.display = "flex";
	document.getElementById("user").innerHTML = list[1];
    }//end if
}//end authenticateRedirect()

/**
 * Send the order to server
 */
function sendOrderToServer(){
    //populate the list with javascript string objects from localStorage
    var list = [];
    for(var i=0; i<localStorage.length; i++){
	key = localStorage.key(i);
	if(key.indexOf("item") == 0){
	    list.push(localStorage.getItem(key));
	}//end if
    }//end for
    
    //push user comments in the list
    var comment;
    for(var i=localStorage.length-1; i>=0; i--){
    	key = localStorage.key(i);
    	if(key.indexOf("comment") == 0){
    		comment = localStorage.getItem(key);
    		beforeLastElement = new menuItem(comment, "", 0,0,99999);
    		list.push(JSON.stringify(beforeLastElement));
    		break;
    	}
    }
    
    //the Server needs the user's email to process the order. Last element in the list will be containing user's email
    var cookieString = getCookie("userName");
    var cookieArray = cookieString.split("---");
    var userEmail = cookieArray[0];
    lastElement = new menuItem(userEmail, "", 0, 0, 99999);
    list.push(JSON.stringify(lastElement));
    console.log(list);
    
    //send the list of object to the server
    var xmlhttp;
    if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();
    }else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function(){
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
	  response = xmlhttp.responseText;
	  if(response == "ok"){
	      //Delete orders from local storage and updathe badge
	      localStorage.clear();
	      updateBadge();
	      var title = "Notice:";
	      var body = '<b>Your order has been processed successfully.</b><br><br>Click "Ok" to continue to User area.<br>Thank you for using our service.';
	      popup = '<div id="popup_background"><div id="popup_box"><div id="popup_title"><p>'+
	      title+'</p></div><div id="popup_message"><p>'+
	      body+'</p></div><div id="popup_buttons">'+
		  '<input type="submit" id="popup_buttonAffirmative" value="OK" onclick="window.location = \'userArea.html\'">'+
		  '</div></div></div>"';
		node = document.createElement("div");
		node.innerHTML = popup;
		document.body.appendChild(node);
	  }else{
	      alert("DB connection error");
	  }
        }//end if
	};//end function
	xmlhttp.open("POST","rest/sendOrder",true);
	xmlhttp.setRequestHeader("Content-type","application/json");
	xmlhttp.send("["+list+"]");
}//end sendOrder()

/**
 * Load and populate user details
 */
function populateUserDetails(){
    var userCookie = getCookie("userName");
    var email;
    if (userCookie != null){
	var list = userCookie.split("---");
	email = list[0];
    }else{return;}
    
    var xmlhttp;
    if (window.XMLHttpRequest){xmlhttp=new XMLHttpRequest();
    }else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function(){
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
	  response = xmlhttp.responseText;
	  parsedResponse = JSON.parse(response);
	  if(typeof parsedResponse === "object"){
	      document.getElementById("name").value = parsedResponse[0][1];
	      document.getElementById("surname").value = parsedResponse[0][2];
	      document.getElementById("phone").value = parsedResponse[0][3];
	      document.getElementById("address").value = parsedResponse[0][5];
	      document.getElementById("oldEmail").value = parsedResponse[0][0];
	  }else{
	      alert(response);
	  }
	  
      }
    };
    xmlhttp.open("GET","rest/getUserByEmail?email="+email,true);
    xmlhttp.send();
    populateOrderSummary();
}

/**
 * Update user details
 */
function updateUserDetails(){
    document.getElementById("updateError").style.display = "none";
    var nameString = document.getElementById("name").value;
    var surnameString = document.getElementById("surname").value;
    var phoneString = document.getElementById("phone").value;
    var addressString = document.getElementById("address").value;
    var oldEmailString = document.getElementById("oldEmail").value;
    var emailString = document.getElementById("email").value;
    var emailConfirmString = document.getElementById("emailConfirm").value;
    var currentPasswordString = document.getElementById("passwordCurrent").value;
    var passwordString = document.getElementById("password").value;
    var passwordConfirmString = document.getElementById("passwordConfirm").value;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var newEmailHasBeenSet = false;
    var newPasswordHasBeenSet = false;
    var POSTString;
    
    //validation
    if(!(document.getElementById("name").checkValidity() &&
    		document.getElementById("surname").checkValidity() &&
    		document.getElementById("phone").checkValidity() &&
    		document.getElementById("address").checkValidity() &&
    		document.getElementById("passwordCurrent").checkValidity()))
    	return;
    document.getElementById("email").setCustomValidity("");
    if(emailString != "" || emailConfirmString != ""){
		if (emailString != emailConfirmString){
		    compare("email");
		    return;
		}
		if(re.test(emailString) == false){
			document.getElementById("email").setCustomValidity("Invalid Email!");
			return;
		}
		newEmailHasBeenSet = true;
    }
    if(passwordString != "" || passwordConfirmString != ""){
		if (passwordString != passwordConfirmString){
		    compare("password");
		    return;
		    };
		if (passwordString.length < 6){
		    document.getElementById("passwordConfirm").setCustomValidity("Password must be at least 6 characters long");
		    return;
		}else{
		    document.getElementById("passwordConfirm").setCustomValidity("");
		}
		newPasswordHasBeenSet = true;
    }
    if(currentPasswordString == ""){return;}
    if (currentPasswordString.length < 6){
	    document.getElementById("passwordCurrent").setCustomValidity("Password must be at least 6 characters long");
	    return;
	}else{
	    document.getElementById("passwordCurrent").setCustomValidity("");
	}
    
    if(newEmailHasBeenSet && newPasswordHasBeenSet){
	POSTString = "name="+encodeURIComponent(nameString)+"&surname="+encodeURIComponent(surnameString)+"&phone="+encodeURIComponent(phoneString)+
	"&address="+encodeURIComponent(addressString)+"&newEmail="+encodeURIComponent(emailString)+"&newPassword="+encodeURIComponent(passwordString)+
	"&currentEmail="+encodeURIComponent(oldEmailString)+"&currentPassword="+encodeURIComponent(currentPasswordString);
    }else if(newEmailHasBeenSet){
	POSTString = "name="+encodeURIComponent(nameString)+"&surname="+encodeURIComponent(surnameString)+"&phone="+encodeURIComponent(phoneString)+
	"&address="+encodeURIComponent(addressString)+"&newEmail="+encodeURIComponent(emailString)+"&newPassword="+encodeURIComponent(currentPasswordString)+
	"&currentEmail="+encodeURIComponent(oldEmailString)+"&currentPassword="+encodeURIComponent(currentPasswordString);
    }else if(newPasswordHasBeenSet){
	POSTString = "name="+encodeURIComponent(nameString)+"&surname="+encodeURIComponent(surnameString)+"&phone="+encodeURIComponent(phoneString)+
	"&address="+encodeURIComponent(addressString)+"&newEmail="+encodeURIComponent(oldEmailString)+"&newPassword="+encodeURIComponent(passwordString)+
	"&currentEmail="+encodeURIComponent(oldEmailString)+"&currentPassword="+encodeURIComponent(currentPasswordString);
    }else{
	POSTString = "name="+encodeURIComponent(nameString)+"&surname="+encodeURIComponent(surnameString)+"&phone="+encodeURIComponent(phoneString)+
	"&address="+encodeURIComponent(addressString)+"&newEmail="+encodeURIComponent(oldEmailString)+"&newPassword="+encodeURIComponent(currentPasswordString)+
	"&currentEmail="+encodeURIComponent(oldEmailString)+"&currentPassword="+encodeURIComponent(currentPasswordString);
    }
    
    var xmlhttp;
    if (window.XMLHttpRequest) {
	xmlhttp = new XMLHttpRequest();
    } else {
	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    var response = xmlhttp.responseText;
	    var parsedResponse;
	    try{
		parsedResponse = JSON.parse(response);
		console.log("User has been updated in the DB");
		setCookie("userName",parsedResponse[0][0]+"---"+parsedResponse[0][1],3);
		userCheck();
		popup = '<div id="popup_background"><div id="popup_box"><div id="popup_title"><p>Update:</p></div><div id="popup_message"><p>Your account has been updated successfuly</p></div><div id="popup_buttons">'+
		  '<input type="submit" id="popup_buttonAffirmative" value="OK" onclick="document.getElementById(\'popup_background\').style.display = \'none\'">'+
		  '</div></div></div>"';
		node = document.createElement("div");
		node.innerHTML = popup;
		document.body.appendChild(node);
	    }catch (error){
		document.getElementById("updateError").style.display = "block";
		if(response.search("incorrect password"))
			document.getElementById("updateError").innerHTML = "Incorrect Password";
		else
			document.getElementById("updateError").innerHTML = response;
	    }
	}
    };
    xmlhttp.open("POST","rest/updateUser",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(POSTString);
}

/**
 * Populates the previous orders of the users
 */
function populateOrderSummary(){
    var userCookie = getCookie("userName");
    var email;
    if (userCookie != null){
	var list = userCookie.split("---");
	email = list[0];
    }else{return;}
    
    var xmlhttp;
    if (window.XMLHttpRequest){	xmlhttp=new XMLHttpRequest();
    }else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function(){
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
	  result = xmlhttp.responseText;
	  var parsedResult;
	  try{
	      parsedResult = JSON.parse(result);
	      for(var i=0; i<parsedResult.length; i++){
	    	  // From http://www.webdevelopersnotes.com/tips/html/javascript_date_and_time.php3
	    	  var m_names = new Array("January", "February", "March", 
	    			  "April", "May", "June", "July", "August", "September", 
	    			  "October", "November", "December");
			  var date = new Date(parsedResult[i].added);
			  var node = document.createElement("tr");
			  calendar_date = date.getDate();
			  hours = date.getHours();
			  minutes = date.getMinutes();
			  if(calendar_date < 10){
				  calendar_date_string = "0"+calendar_date;
			  }else{
				  calendar_date_string = calendar_date;
			  }
			  if(hours < 10){
				  hours_string = "0"+hours;
			  }else{
				  hours_string = hours;
			  }
			  if(minutes < 10){
				  minutes_string = "0"+minutes;
			  }else{
				  minutes_string = minutes;
			  }
			  node.innerHTML = "<td>"+calendar_date_string+"/"+m_names[date.getMonth()]+"/"+date.getFullYear()+" at "+hours_string+":"+minutes_string+"</td><td>"+parsedResult[i].items+"</td><td>"+parsedResult[i].status+"</td><td>"+parsedResult[i].total+"</td>";
			  document.getElementById("summaryTable").appendChild(node);
	      }
	  }catch (e){
	      alert(e);
	  }
        }//end if
	};//end function
	xmlhttp.open("GET","rest/getSummary?email="+encodeURIComponent(email),true);
	xmlhttp.send();
}

/**
 * Fills the contact form with user's details if user is logged in
 */
function fillContact(){
    var cookie = getCookie("userName");
    try{
	var parts = cookie.split("---");
	document.getElementById("email").value = parts[0];
	document.getElementById("name").value=parts[1];
    }catch (e){
	
    }
}
 
/**
 * Displays the nottification message for adding items to order
 */
function itemAddedPopup(){
    if(document.getElementById("popupItemAdded") != null){
	document.getElementById("popupItemAdded").style.display = "block";
	window.setTimeout(function(){document.getElementById("popupItemAdded").style.display = "none";}, 1000);
}else{
    popup = '<div id="popupItemAdded"><div id="popup_message"><p>Your order has been added to your plate</p></div></div>';
    node = document.createElement("div");
    node.innerHTML = popup;
    document.body.appendChild(node);
    window.setTimeout(function(){document.getElementById("popupItemAdded").style.display = "none";}, 1000);
	}
}

//function slideBox(){
//    var div = document.getElementById("slideBox");
//    var intervalIncrease;
//    var decreaseInterval;
//    var rect = div.getBoundingClientRect();
//    var thisleft = rect.left;
//    var originalLeft = thisleft;
//	intervalIncrease = window.setInterval(
//		function(){
//		thisleft+=2; div.style.left=thisleft+"px";
//		if(thisleft >= originalLeft+100){
//	    		window.clearInterval(intervalIncrease);
//	    		window.setTimeout(
//		    	function(){
//				decreaseInterval = window.setInterval(
//				function(){
//			    	thisleft-=2; div.style.left=thisleft+"px";
//			    	if(thisleft<=originalLeft){
//					window.clearInterval(decreaseInterval);
//					div.style.left = originalLeft+"px";
//				}
//				},1);
//		    	},500);
//		}
//		},1);
//}