package tools;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import model.CompleteItem;
import model.Favorite;
import model.FavoritePK;
import model.Order;
import model.OrderSumary;
import model.Orderdetail;
import model.OrderdetailPK;
import model.Price;
import model.User;

@Stateless
@Path("/")
public class Service {
	
	@PersistenceContext
	private EntityManager em;

	@GET
	@Path("isServerUp")
	@Produces(MediaType.TEXT_PLAIN)
	public String isServerUp(){
		return "Up";
	}
	
	@GET
	@Produces("text/plain")
	public String displayMessage()
	{
		return "Hello Zdeno";
	}
	
	@GET
	@Path("menuitem")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getItem(String id){
		try{
		@SuppressWarnings("unchecked")
		List<Object> list = (List<Object>) em.createQuery("SELECT p.id, p.price, p.item.name FROM Price AS p").getResultList();
		if(list.isEmpty()){
			return Response.ok().entity("Nothing to return").build();
		}
		return Response.ok().entity(list).build();
		}catch (Exception e){
			e.printStackTrace();
			return Response.ok().entity("Database error!").build();
		}//end catch
	}//end getItem
	
	@GET
	@Path("getUserByEmail")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByEmail(@QueryParam("email") String email){
		try{
		Query query = em.createQuery("SELECT u.email, u.name, u.surname, u.phone, u.password, u.address, u.balance FROM User u WHERE u.email = :stringEmail");
		query.setParameter("stringEmail", email);
		@SuppressWarnings("unchecked")
		List<Object> list = (List<Object>) query.getResultList();
		if(list.isEmpty()){
			return Response.ok().entity("User not found/Incorrect Password").build();
		}
		return Response.ok().entity(list).build();
		}catch (Exception e){
			e.printStackTrace();
			return Response.ok().entity("Database error!").build();
		}//end catch
	}//end getUserByEmail
	
	@GET
	@Path("getUserByEmailAndPassword")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserByEmailAndPassword(@QueryParam("email") String email, @QueryParam("password") String password){
		if (em == null){
			return Response.ok().entity(Status.INTERNAL_SERVER_ERROR).build();
		}
		try{
			//I am using mysql encryption which cannot be called from JPA so we will run a stored procedure on MySql
			//returns email, name, surname, phone, password, address, balance
			Query query = em.createNativeQuery("{CALL getUser(?,?)}"); 
			query.setParameter(1, email);
			query.setParameter(2, password);
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) query.getResultList();
			if(list.isEmpty()){
				return Response.ok().entity("User not found").build();
			}
			return Response.ok().entity(list).build();
			}catch (Exception e){
				e.printStackTrace();
				return Response.ok().entity("Database error!").build();
			}//end catch
	}//end getUserByEmailAndPassword()
	
	@POST
	@Path("registerUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response registerUser(@FormParam("name") String name,@FormParam("surname") String surname,
			@FormParam("phone") String phone, @FormParam("address") String address,
			@FormParam("email") String email, @FormParam("password") String password){
		if (em == null){
			return Response.ok().entity(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		//perform server side validation
		if(email.equalsIgnoreCase("") || name.equalsIgnoreCase("") || surname.equalsIgnoreCase("") || 
				phone.equalsIgnoreCase("") || address.equalsIgnoreCase("") || password.equalsIgnoreCase("")){
			return Response.ok().entity("Form fields cannot be empty.").build();
		}else if(!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
			return Response.ok().entity("Invalid Email").build();
		}else if(password.length() < 6){
			return Response.ok().entity("Password cannot be less than 6 characters long.").build();
		}else if(!phone.matches("[0-9\\s\\-\\(\\)\\+]{9,15}")){
			return Response.ok().entity("Invalid phone number.").build();
		}
		
		try{
			//I am using mysql encryption which cannot be called from JPA so we will run a stored procedure on MySql
			Query query = em.createNativeQuery("{CALL new_user(?1,?2,?3,?4,?5,?6)}");
			query.setParameter(1, email);
			query.setParameter(2, name);
			query.setParameter(3, surname);
			query.setParameter(4, phone);
			query.setParameter(5, address);
			query.setParameter(6, password);
			query.executeUpdate();
			return Response.ok().entity("OK").build();
			}catch (Exception e){
				return Response.ok().entity("A user with the same email is already registered.").build();
			}//end catch
	}//end registerUser()
	
	@GET
	@Path("getAllItems")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllItems(){
		try{
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>) em.createQuery("SELECT p.id, p.price, p.size, p.item.name, p.item.allergy, p.item.category,"
					+ "p.item.descriptionLong, p.item.descriptionShort, p.item.grouping, p.item.imageName, p.item.ingredients, n.carbs, n.fat, "
					+ "n.fibre, n.kcalories, n.protein, n.salt, n.saturates, n.sugar FROM Price p, Nutrition n WHERE p.item.name = n.itemName AND p.available = 1").getResultList();
			ArrayList<CompleteItem> arrayList = new ArrayList<CompleteItem>();
			
			for(Object[] o:list){
				CompleteItem c = new CompleteItem();
				c.setId((Integer) o[0]);
				c.setPrice( (BigDecimal) o[1]);
				c.setSize((String) o[2]);
				c.setName((String) o[3]);
				c.setAllergy((String) o[4]);
				c.setCategory((String) o[5]);
				c.setDescriptionLong((String) o[6]);
				c.setDescriptionShort((String) o[7]);
				c.setGrouping((String) o[8]);
				c.setImageName((String) o[9]);
				c.setIngredients((String) o[10]);
				c.setCarbs((BigDecimal) o[11]);
				c.setFat((BigDecimal) o[12]);
				c.setFibre((BigDecimal)o[13]);
				c.setKcalories((BigDecimal)o[14]);
				c.setProtein((BigDecimal)o[15]);
				c.setSalt((BigDecimal)o[16]);
				c.setSaturates((BigDecimal)o[17]);
				c.setSugar((BigDecimal)o[18]);
				arrayList.add(c);
			}
			
			if(arrayList.isEmpty()){
				return Response.ok().entity("Nothing to return").build();
			}
			return Response.ok().entity(arrayList).build();
		}catch (Exception e){
			//return Response.ok().entity(Status.NO_CONTENT).build();
			return Response.ok().entity(e.toString()).build();
		}//end catch
	}//end getAllItems()
	
	@GET
	@Path("getItemCategories")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getItemCategories(){
		try{
			return Response.ok().entity(em.createQuery("SELECT DISTINCT(i.category) FROM Item i ORDER BY i.category DESC").getResultList()).build();
		}catch (Exception e){
			return Response.ok().entity(Status.NO_CONTENT).build();
		}//end catch
	}//end getItemCategories()
	
	@GET
	@Path("getAllItemsWithDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllItemsWithDetails(){
		try{
			Query query = em.createQuery("SELECT d FROM DescriptionItem d GROUP BY d.name");
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) query.getResultList();
			if(list.isEmpty()){
				return Response.ok().entity("No Items found").build();
			}
			return Response.ok().entity(list).build();
		}catch (Exception e){
			e.printStackTrace();
			return Response.ok().entity("Database error!").build();
		}//end catch
	}//end getAllItemsWithDetails()
	
	@GET
	@Path("getAllItemsFromPriceTable")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllItemsFromPriceTable(@QueryParam("itemName") String itemName){
		try{
			Query query = em.createQuery("SELECT p.id, p.item.name, p.size, p.price, p.available FROM Price p WHERE p.item.name=:itemName");
			query.setParameter("itemName", itemName);
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) query.getResultList();
			if(list.isEmpty()){
				return Response.ok().entity("No Prices found").build();
			}
			return Response.ok().entity(list).build();
			}catch (Exception e){
				e.printStackTrace();
				return Response.ok().entity("Database error!").build();
			}//end catch
	}//end getAllItemsFromPriceTable
	
	@GET
	@Path("getSpecialOffer")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSpecialOffer(){
		try{
			Query query = em.createQuery("SELECT DISTINCT i.name, i.descriptionShort, i.imageName, p.price FROM Item i, Price p WHERE p.item.name = i.name GROUP BY p.item.name ORDER BY RAND()").setMaxResults(4);
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) query.getResultList();
			return Response.ok().entity(list).build();
			}catch (Exception e){
				e.printStackTrace();
				return Response.ok().entity("Database error!").build();
			}//end catch
	}//end getSpecialOffer()
	
	@POST
	@Path("email/sendEmail")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String sendThisEmail(@FormParam("FROM") String from, @FormParam("NAME") String name, @FormParam("PHONE") String phone, @FormParam("MESSAGE") String text){
		
		final String username = "c.zdeno@gmail.com";
		final String password = "Ild1k0kAM1la";
		final String to = "VirtualDinerProject@mailinator.com";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject("Virtual Diner Contact");
			message.setContent("<body><b>FROM: </b>"+name+"<br/><b>PHONE: </b>"+phone+"<br/><b>CONTENT: </b><pre>"+text+"</pre></body>", "text/html; charset=utf-8");

			Transport.send(message);

			System.out.println("Done");
			return "OK";

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}//end send email
	
	@POST
	@Path("email/sendEmail/resetPassword")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String resetPassword(@FormParam("email") String email){
		if (em == null){
			System.out.println("EM is null");
			return "EM not working";
		}
		
		//generates a random string
		String uuid = UUID.randomUUID().toString();
		//call resetPassword mysql stored function to reset the pasword
		Query query = em.createNativeQuery("{call resetPassword('"+uuid.substring(0, 6)+"','"+email+"')}");
		int updatedRows = query.executeUpdate();
		//check if the reset has been successful(valid email address has been given)
		if(updatedRows == 1){
			final String username = "c.zdeno@gmail.com";
			final String password = "Ild1k0kAM1la";
			final String from = "VirtualDinerProject@mailinator.com";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });

			try {
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
				message.setSubject("Virtual Diner Password reset");
				message.setContent("<body>This is an automated message, please do not reply<br/>"+
				   "Your password has been reset. This is your temporary password: <b>"+uuid.substring(0, 6)+"</b><br/>"+
				   "Login with this password and changi afterwards.</body>", "text/html; charset=utf-8");
				
				Transport.send(message);

				System.out.println("Done");
				return "OK";

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
		}else{
			return "The email you have provided is not in the databse";
		}
	}//end resetPassword
	
	//sendOrder to DB
	@POST
	@Path("sendOrder")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendOrder(List<UserOrder> list){
		
		//CREATE DB INSERTS TO POPULATE TABLES
		Date date = new Date();
		Timestamp now = new Timestamp(date.getTime());
		int orderId = 0;
		Order order = new Order();
		order.setType("user");
		order.setTotal(0.0);
		order.setStatus("done");
		order.setAdded(now);
		order.setTableid(list.get(0).getTableId());
		//add user email which is stored in the list as the last item, where the value of the email is saved as the name of the item
		order.setUser(em.find(User.class, list.get(list.size()-1).getName()));
		//add comments which are stored in the list as the before-last item, where the text of the comment is saved as the name of the item
		order.setComment(list.get(list.size()-2).getName());
		em.persist(order);
		orderId = order.getIdorder();
		
		double total = 0;
		for(int i=0; i<list.size()-2; i++){
			Orderdetail od = new Orderdetail();
			OrderdetailPK pk = new OrderdetailPK(order.getIdorder(), list.get(i).getId());
			od.setId(pk);
			od.setQuantity(list.get(i).getAmount());	
			total+=list.get(i).getTotal();
			em.persist(od);
		}
		Order order1 = em.find(Order.class, orderId);
		order1.setTotal(total);
//		em.refresh(order1);
		
		return "ok";
	}
	
	//sendOrder to DB
	@POST
	@Path("sendOrderAsUnknownCustomer")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String sendOrderAsUnknownCustomer(List<UserOrder> list){
		
		//CREATE DB INSERTS TO POPULATE TABLES
		Date date = new Date();
		Timestamp now = new Timestamp(date.getTime());
		int orderId = 0;
		Order order = new Order();
		order.setType("customer");
		order.setTotal(0.0);
		order.setStatus("done");
		order.setAdded(now);
		order.setTableid(list.get(0).getTableId());
		//add comments which are stored in the list as the last item, where the text of the comment is saved as the name of the item
		order.setComment(list.get(list.size()-1).getName());
		em.persist(order);
		orderId = order.getIdorder();
		
		double total = 0;
		for(int i=0; i<list.size()-1; i++){
			Orderdetail od = new Orderdetail();
			OrderdetailPK pk = new OrderdetailPK(order.getIdorder(), list.get(i).getId());
			od.setId(pk);
			od.setQuantity(list.get(i).getAmount());	
			total+=list.get(i).getTotal();
			em.persist(od);
		}
		Order order1 = em.find(Order.class, orderId);
		order1.setTotal(total);
//			em.refresh(order1);
		
		return "ok";
	}
	
	@POST
	@Path("updateUser")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateUser(@FormParam("name") String name,@FormParam("surname") String surname,
			@FormParam("phone") String phone, @FormParam("address") String address,
			@FormParam("newEmail") String newEmail, @FormParam("newPassword") String newPassword,
			@FormParam("currentEmail") String currentEmail, @FormParam("currentPassword") String currentPassword){
		if (em == null){
			return Response.ok().entity(Status.INTERNAL_SERVER_ERROR).build();
		}
		
		//perform server side validation
		if(newEmail.equalsIgnoreCase("") || name.equalsIgnoreCase("") || surname.equalsIgnoreCase("") || 
				phone.equalsIgnoreCase("") || address.equalsIgnoreCase("") || newPassword.equalsIgnoreCase("") || 
				currentEmail.equalsIgnoreCase("") || currentPassword.equalsIgnoreCase("")){
			return Response.ok().entity("Form fields cannot be empty.").build();
		}else if(!newEmail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") ||
				!currentEmail.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
			return Response.ok().entity("Invalid Email").build();
		}else if(newPassword.length() < 6 || currentPassword.length() < 6){
			return Response.ok().entity("Password cannot be less than 6 characters long.").build();
		}else if(!phone.matches("[0-9\\s\\-\\(\\)\\+]{9,15}")){
			return Response.ok().entity("Invalid phone number.").build();
		}
		
		System.out.println("Name = "+name+" surname = "+surname+" New Email = "+newEmail+" newPassword = "+newPassword+" currentEmail = "+currentEmail+" currentPassword = "+currentPassword);
		try{
				Query query = em.createNativeQuery("{CALL updateUser(?1,?2,?3,?4,?5,?6,?7,?8)}");
				query.setParameter(1, newEmail);
				query.setParameter(2, name);
				query.setParameter(3, surname);
				query.setParameter(4, phone);
				query.setParameter(5, address);
				query.setParameter(6, newPassword);
				query.setParameter(7, currentEmail);
				query.setParameter(8, currentPassword);
				@SuppressWarnings("unchecked")
				List<Object> list = (List<Object>) query.getResultList();
				return Response.ok().entity(list).build();
			}catch (Exception e){
				return Response.ok().entity(e.getMessage()).build();
			}
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("getSummary")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummary(@QueryParam("email") String email){
		Query query = em.createQuery("SELECT o FROM Order o WHERE o.user.email = :email ORDER BY o.idorder DESC").setMaxResults(15);
		query.setParameter("email", email);
		@SuppressWarnings("unchecked")
		List<Order> orders = (List<Order>) query.getResultList();
		
		OrderSumary orderSumary[] = new OrderSumary[orders.size()];
		//System.out.println(((Order)orders.get(0)).getIdorder());
		for(int i=0; i<orders.size(); i++){
			int currentLoopOrder = orders.get(i).getIdorder();
			Query query1 = em.createQuery("SELECT CONCAT(p.item.name,' (',od.quantity,')') FROM Price p, Orderdetail od WHERE p.id = od.price.id AND od.order.idorder = :currentLoopOrder");
			query1.setParameter("currentLoopOrder", currentLoopOrder);
			List<Object> summary = query1.getResultList();
			orderSumary[i] = new OrderSumary();
			orderSumary[i].setIdorder(orders.get(i).getIdorder());
			orderSumary[i].setStatus(orders.get(i).getStatus());
			orderSumary[i].setTotal(orders.get(i).getTotal());
			orderSumary[i].setAdded(orders.get(i).getAdded());
			for(int j=0; j<summary.size(); j++){
				if(orderSumary[i].getItems() == null || orderSumary[i].getItems() == ""){
					orderSumary[i].setItems(summary.get(j).toString());
				}else{
					orderSumary[i].setItems(orderSumary[i].getItems()+"<br>"+summary.get(j));
				}
			}
		}
		return Response.ok().entity(orderSumary).build();
	}
	
	@POST
	@Path("setFavorite")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String setFavorite(@FormParam("email") String email,@FormParam("itemID") int itemID){
		try{
			Query q = em.createNativeQuery("SELECT * FROM favorites WHERE userEmail=:email AND itemID=:itemID");
			q.setParameter("email", email);
			q.setParameter("itemID", itemID);
			List<Object> results = q.getResultList();
			if(results.isEmpty()){
				FavoritePK fpk = new FavoritePK();
				fpk.setItemID(itemID);
				fpk.setUserEmail(email);
				Favorite f = new Favorite();
				f.setId(fpk);
				em.persist(f);
				return "OK";
			}else{
				return "DUPLICATE";
			}
		}catch (Exception e){
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	@POST
	@Path("deleteFavorite")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String deleteFavorite(@FormParam("email") String email,@FormParam("itemName") String itemName){
		try{
			Query query = em.createQuery("SELECT p FROM Price p WHERE p.item.name =:itemName",Price.class).setMaxResults(1);
			query.setParameter("itemName", itemName);
			Price price = (Price) query.getSingleResult();
			int itemId = price.getId();
			
			Query q = em.createQuery("SELECT f FROM Favorite f WHERE f.id.itemID=:idUser and f.id.userEmail=:userEmail",Favorite.class);
			q.setParameter("idUser", itemId);
			q.setParameter("userEmail", email);
			
			Favorite fav = (Favorite) q.getSingleResult();
			em.remove(fav);
			return "OK";
			
		}catch (Exception e){
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Path("fetchFavorites")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchFavorites(@QueryParam("email") String email){
		try{
		Query query = em.createNativeQuery("SELECT * FROM favorites WHERE userEmail=:email");
		query.setParameter("email", email);
		@SuppressWarnings("unchecked")
		List<Object[]> list = (List<Object[]>) query.getResultList();
		return Response.ok().entity(list).build();
		}catch (Exception e){
			e.printStackTrace();
			return Response.ok().entity("ERROR").build();
		}
		
	}
	
	
}// end class Service


