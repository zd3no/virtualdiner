package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the nutritions database table.
 * 
 */
@Entity
@Table(name="nutritions")
@NamedQuery(name="Nutrition.findAll", query="SELECT n FROM Nutrition n")
public class Nutrition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="item_name", unique=true, nullable=false, length=45)
	private String itemName;

	@Column(precision=10, scale=1)
	private BigDecimal carbs;

	@Column(precision=10, scale=1)
	private BigDecimal fat;

	@Column(precision=10, scale=1)
	private BigDecimal fibre;

	@Column(precision=10)
	private BigDecimal kcalories;

	@Column(precision=10, scale=1)
	private BigDecimal protein;

	@Column(precision=10, scale=1)
	private BigDecimal salt;

	@Column(precision=10, scale=1)
	private BigDecimal saturates;

	@Column(precision=10, scale=1)
	private BigDecimal sugar;

	//bi-directional one-to-one association to Item
	@OneToOne
	@JoinColumn(name="item_name", nullable=false, insertable=false, updatable=false)
	private Item item;

	public Nutrition() {
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public BigDecimal getCarbs() {
		return this.carbs;
	}

	public void setCarbs(BigDecimal carbs) {
		this.carbs = carbs;
	}

	public BigDecimal getFat() {
		return this.fat;
	}

	public void setFat(BigDecimal fat) {
		this.fat = fat;
	}

	public BigDecimal getFibre() {
		return this.fibre;
	}

	public void setFibre(BigDecimal fibre) {
		this.fibre = fibre;
	}

	public BigDecimal getKcalories() {
		return this.kcalories;
	}

	public void setKcalories(BigDecimal kcalories) {
		this.kcalories = kcalories;
	}

	public BigDecimal getProtein() {
		return this.protein;
	}

	public void setProtein(BigDecimal protein) {
		this.protein = protein;
	}

	public BigDecimal getSalt() {
		return this.salt;
	}

	public void setSalt(BigDecimal salt) {
		this.salt = salt;
	}

	public BigDecimal getSaturates() {
		return this.saturates;
	}

	public void setSaturates(BigDecimal saturates) {
		this.saturates = saturates;
	}

	public BigDecimal getSugar() {
		return this.sugar;
	}

	public void setSugar(BigDecimal sugar) {
		this.sugar = sugar;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}