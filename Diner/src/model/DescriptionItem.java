package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the descriptionItems database table.
 * 
 */
@Entity
@IdClass(DescItemCompositePK.class)
@Table(name="descriptionItems")
@NamedQuery(name="DescriptionItem.findAll", query="SELECT d FROM DescriptionItem d")
public class DescriptionItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length=45)
	private String allergy;

	private byte available;

	@Column(precision=10, scale=1)
	private BigDecimal carbs;

	@Column(nullable=false, length=1)
	private String category;

	@Lob
	@Column(name="description_long")
	private String descriptionLong;

	@Column(name="description_short", length=45)
	private String descriptionShort;

	@Column(precision=10, scale=1)
	private BigDecimal fat;

	@Column(precision=10, scale=1)
	private BigDecimal fibre;

	@Column(name="image_name", length=45)
	private String imageName;

	@Lob
	private String ingredients;

	@Column(precision=10)
	private BigDecimal kcalories;

	@Id
	@Column(nullable=false, length=45)
	private String name;

	@Column(precision=10, scale=2)
	private BigDecimal price;

	@Column(precision=10, scale=1)
	private BigDecimal protein;

	@Column(precision=10, scale=1)
	private BigDecimal salt;

	@Column(precision=10, scale=1)
	private BigDecimal saturates;

	@Id
	@Column(nullable=false, length=1)
	private String size;

	@Column(precision=10, scale=1)
	private BigDecimal sugar;

	public DescriptionItem() {
	}

	public String getAllergy() {
		return this.allergy;
	}

	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}

	public byte getAvailable() {
		return this.available;
	}

	public void setAvailable(byte available) {
		this.available = available;
	}

	public BigDecimal getCarbs() {
		return this.carbs;
	}

	public void setCarbs(BigDecimal carbs) {
		this.carbs = carbs;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescriptionLong() {
		return this.descriptionLong;
	}

	public void setDescriptionLong(String descriptionLong) {
		this.descriptionLong = descriptionLong;
	}

	public String getDescriptionShort() {
		return this.descriptionShort;
	}

	public void setDescriptionShort(String descriptionShort) {
		this.descriptionShort = descriptionShort;
	}

	public BigDecimal getFat() {
		return this.fat;
	}

	public void setFat(BigDecimal fat) {
		this.fat = fat;
	}

	public BigDecimal getFibre() {
		return this.fibre;
	}

	public void setFibre(BigDecimal fibre) {
		this.fibre = fibre;
	}

	public String getImageName() {
		return this.imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getIngredients() {
		return this.ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public BigDecimal getKcalories() {
		return this.kcalories;
	}

	public void setKcalories(BigDecimal kcalories) {
		this.kcalories = kcalories;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getProtein() {
		return this.protein;
	}

	public void setProtein(BigDecimal protein) {
		this.protein = protein;
	}

	public BigDecimal getSalt() {
		return this.salt;
	}

	public void setSalt(BigDecimal salt) {
		this.salt = salt;
	}

	public BigDecimal getSaturates() {
		return this.saturates;
	}

	public void setSaturates(BigDecimal saturates) {
		this.saturates = saturates;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public BigDecimal getSugar() {
		return this.sugar;
	}

	public void setSugar(BigDecimal sugar) {
		this.sugar = sugar;
	}

}