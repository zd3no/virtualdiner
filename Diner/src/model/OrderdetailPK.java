package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the orderdetails database table.
 * 
 */
@Embeddable
public class OrderdetailPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int idorder;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int iditem;

	public OrderdetailPK() {
	}
	
	public OrderdetailPK(int idOrder, int idItem) {
		this.idorder = idOrder;
		this.iditem = idItem;
	}
	
	public int getIdorder() {
		return this.idorder;
	}
	public void setIdorder(int idorder) {
		this.idorder = idorder;
	}
	public int getIditem() {
		return this.iditem;
	}
	public void setIditem(int iditem) {
		this.iditem = iditem;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof OrderdetailPK)) {
			return false;
		}
		OrderdetailPK castOther = (OrderdetailPK)other;
		return 
			(this.idorder == castOther.idorder)
			&& (this.iditem == castOther.iditem);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idorder;
		hash = hash * prime + this.iditem;
		
		return hash;
	}
}