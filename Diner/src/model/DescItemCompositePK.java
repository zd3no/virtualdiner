package model;

import java.io.Serializable;

public class DescItemCompositePK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String name;
	String size;
	
	public DescItemCompositePK(){
		
	}
	
	public DescItemCompositePK(String name, String size) {
        this.name = name;
        this.size = size;
    }
 
    public String getName() {
        return name;
    }
 
    public String getSize() {
        return size;
    }
 
    // Must have a hashCode method
    @Override
    public int hashCode() {
    	String x = name + size;
        return x.hashCode();
    }
 
    // Must have an equals method
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DescItemCompositePK) {
        	DescItemCompositePK item = (DescItemCompositePK) obj;
            return item.name == this.name && item.size.equals(this.size);
        }
 
        return false;
    }
	

}
