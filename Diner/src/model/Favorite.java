package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the favorites database table.
 * 
 */
@Entity
@Table(name="favorites")
@NamedQuery(name="Favorite.findAll", query="SELECT f FROM Favorite f")
public class Favorite implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FavoritePK id;

	public Favorite() {
	}

	public FavoritePK getId() {
		return this.id;
	}

	public void setId(FavoritePK id) {
		this.id = id;
	}

}