package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the favorites database table.
 * 
 */
@Embeddable
public class FavoritePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false, unique=true, nullable=false, length=45)
	private String userEmail;

	@Column(insertable=false, updatable=false, unique=true, nullable=false)
	private int itemID;

	public FavoritePK() {
	}
	public String getUserEmail() {
		return this.userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public int getItemID() {
		return this.itemID;
	}
	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FavoritePK)) {
			return false;
		}
		FavoritePK castOther = (FavoritePK)other;
		return 
			this.userEmail.equals(castOther.userEmail)
			&& (this.itemID == castOther.itemID);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.userEmail.hashCode();
		hash = hash * prime + this.itemID;
		
		return hash;
	}
}